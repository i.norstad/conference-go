from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        'name',
    ]

# for showing a list AND updating a list:
@require_http_methods(['GET', 'POST'])
def api_list_conferences(request):
    if request.method == 'GET':
        conferences = Conference.objects.all()
        return JsonResponse(
            {'conferences': conferences},
            encoder=ConferenceListEncoder,
            safe=False
        )
    # if request.method == 'POST':
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content['location'])
            content['location'] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid location id'},
                status=400,
            )
        # time to do the actual creating:
        conference = Conference.objects.create(**content)
        # return the new creation:
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False
        )


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        'name',
        'photo_url'
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        'weather',
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        'location',
    ]
    encoders = {
        'location': LocationListEncoder(),
    }


@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_conference(request, id):
    if request.method == 'GET':
        try:
            conference = Conference.objects.get(id=id)
            # Use insomnia to pull up what is being returned
            # Get city and state of conference to pass to get_weather()
            # access through conference.location, save as variable
            loc = Location.objects.get(name=conference.location)
            # then use that variable to access location.city, location.state from location details
            # turn state abbreviation into full state name, pass that to get_weather
            weather = get_weather(loc.city, loc.state.name)
            # bind conference weather to weather
            conference.weather = weather

            return JsonResponse(
                conference,
                # this single encoder line means we don't need the commented out properties below:
                encoder=ConferenceDetailEncoder,
                safe=False
            )
        except Conference.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid conference number'},
                status=400
            )

    elif request.method == 'DELETE':
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})

    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content['location'] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": 'Invalid location id'},
                status=400,
            )

        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False
        )


@require_http_methods(['GET', 'POST'])
def api_list_locations(request):

    if request.method == 'GET':
        locations = Location.objects.all()
        return JsonResponse(
            {'locations': locations},
            encoder=LocationListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        # try to load content, but be prepared in case the data has errors in it
        try:
            # Get the State object and put it in the content dict:
            state = State.objects.get(abbreviation=content['state'])
            content['state'] = state
            # handle error response if state abbreviation is incorrect:
        except State.DoesNotExist:
            return JsonResponse(
                {'mesage': 'Invalid state abbreviation'},
                status=400,
            )
        # Get photo for this location
        # grab state object, from abbreviation
        photo_url = get_photo(content['city'], state.name)
        content.update(photo_url)

        location = Location.objects.create(**content)

        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        'photo_url',
    ]

    def get_extra_data(self, o):
        return {'state': o.state.abbreviation}


@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_location(request, id):

    if request.method == 'GET':
        try:
            location = Location.objects.get(id=id)

            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False
            )

        except Location.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid Location number'},
                status=400
            )

    elif request.method == 'DELETE':
        count, _ = Location.objects.filter(id=id).delete()
        # return the string 'deleted' with a count of the deleted objects
        return JsonResponse({'deleted': count > 0})

    # if method == 'PUT'
    else:
        # Convert the submitted JSON-formatted string into a dictionary.
        content = json.loads(request.body)

        try:
            if 'state' in content:
                # Convert the state abbreviation into a State, if it exists.
                state = State.objects.get(abbreviation=content['state'])
                # Use that dictionary to update the existing Location.
                content['state'] = state
                # Return the updated Location object
        # error handling:
        except State.DoesNotExist:
            return JsonResponse(
                {'mesage': 'Invalid state abbreviation'},
                status=400,
            )
        # do the actual updating:
        Location.objects.filter(id=id).update(**content)
        # same as what is used in GET above
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False
        )
