from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

# For retrieving images from PEXELS:
def get_photo(city, state):
    # Use 'requests' documenttion for this syntax:
    url = 'https://api.pexels.com/v1/search'
    headers = {'Authorization': PEXELS_API_KEY}
    params = {
        'query': f'{city} {state}',
        'per_page': 1
    }
    response = requests.get(url, headers=headers, params=params)
    # response returns a json object. Need to use next line to convert object:
    parsed_json = json.loads(response.content)
    # print results to console then update return accordingly
    # hone in on the results, return the first one
    photo_url = parsed_json['photos'][0]['src']['original']
    print(photo_url)
    # return the photo url, per the instructions
    return {
        "photo_url": photo_url
    }


# for retrieving weather data:
def get_weather(city, state):
    # get coordinates first using
    url = f'http://api.openweathermap.org/geo/1.0/direct?'
    # params
    params1 = {
        'q': f'{city},{state},001',
        'limit': 1,
        'appid': OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=params1)
    parsed_json = json.loads(response.content)
    lat = parsed_json[0]['lat']
    lon = parsed_json[0]['lon']
    # print(f'lat & loooong {lat}, {lon}')

    # Use coordinates to get weather
    weather_url = f'https://api.openweathermap.org/data/2.5/weather?'

    params = {
        'lat': lat,
        'lon': lon,
        'appid': OPEN_WEATHER_API_KEY,
        'units': 'imperial'
    }
    weather = requests.get(weather_url, params=params)
    parsed_weather = json.loads(weather.content)

    # print(parsed_weather)
    # this turns description into a string, when it should be a key value pair,
    # similar to {'description: description}... play with this
    description = f"description: {parsed_weather['weather'][0]['description']}"
    temp = f"temp: {parsed_weather['main']['temp']}"

    return temp, description
