from json import JSONEncoder
from datetime import datetime
from typing import Any
from django.db.models import QuerySet


# QuerySet handling:
# For dealing with the following error:
# Object of type QuerySet is not JSON serializable
class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        # if object is instance of QuerySet:
        if isinstance(o, QuerySet):
        # convert object to a list
            return list(o)
        return super().default(o)


# Date handling:
class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return super().default(o)


# For ALL DETAIL VIEWS; add in DateEncoder capability too!!!
class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        if isinstance(o, self.model):
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
            d = {}
            # if o has the attribute get_api_url
            if hasattr(o, 'get_api_url'):
            #    then add its return value to the dictionary
                d['href'] = o.get_api_url()
            #    with the key "href"
        #     * for each name in the properties list
                for property in self.properties:
        #         * get the value of that property from the model instance
        #           given just the property name
                    value = getattr(o, property)
                    # check if property exists in encoders dictionary
                    if property in self.encoders:
                        encoder = self.encoders[property]
                        value = encoder.default(value)
        #         * put it into the dictionary with that property name as
        #           the key
                    d[property] = value
                # this is for handling the following 2 edge cases:
                # putting the name of the status in the api_list_presentations
                # and putting the state abbreviation in the response for api_show_location
                d.update(self.get_extra_data(o))
        #       * return the dictionary
                return d
        #   otherwise,
        #       return super().default(o)  # From the documentation
        else:
            return super().default(o)

    # also for the above mentioned edge cases:
    def get_extra_data(self, o):
        return {}
