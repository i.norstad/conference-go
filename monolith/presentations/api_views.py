from django.http import JsonResponse
from .models import Presentation
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.api_views import Conference


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        'title',
        'status',
        'href',
    ]

# get OR create
@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):

    if request.method == 'GET':
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {
                'presentations': presentations
            }, encoder=PresentationListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content['conference'] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid presentation id'},
                status=400,
            )
         # create new presentation FINALLY, bind it with content
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )

    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"presentations": presentations})


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

@require_http_methods(["GET", "PUT", 'DELETE'])
def api_show_presentation(request, id):
    if request.method == 'GET':
        try:
            presentation = Presentation.objects.get(id=id)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False
            )
        except Presentation.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid presentation number'},
                status=400
            )

    elif request.method == 'DELETE':
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})

    else:
        content = json.loads(request.body)
        try:
            if 'conference' in content:
                conference = Conference.objects.get(id=content['conference'])
                content['conference'] = conference

        except Conference.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid conference id'},
                status=400
            )

        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )

    #     {
    #     "presenter_name": presenter.presenter_name,
    #     "company_name": presenter.company_name,
    #     "presenter_email": presenter.presenter_email,
    #     "title": presenter.title,
    #     "synopsis": presenter.synopsis,
    #     "created": presenter.created,
    #     "status": presenter.status.name,
    #     "conference": {
    #         "name": presenter.conference.name,
    #         "href": presenter.conference.get_api_url(),
    #     }
    # }
